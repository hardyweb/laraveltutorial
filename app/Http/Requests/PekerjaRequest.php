<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
		'tajuk' => 'required',
		'bilwaran' => 'required',
		'statuskelas' => 'required',
		'butiran' => 'required',
		'kategoriwaran' => 'required',
		'statuswaran' => 'required',
		'aktiviti' => 'required',
		'klasifikasi' => 'required',
		'skimperkhidmatan' => 'required',
	
		'kup' => 'sometimes', 
		'gredhakiki' => 'required_with:kup',
		'waranfleksi' => 'sometimes',

        'nama' => 'required|unique:jabatans,nama',

        ];
    }



    public function messages()
	{
		return [

			'jabatan.unique' => 'Jabatan ini telah wujud',
			'jabatan.required' => 'Sila pilih jabatan', 
		];
	}

}
