<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Jabatan extends Model
{

	use SoftDeletes;
	protected $dates = ['deleted_at'];

	public function jabatan(){

	return $this->belongsTo('App\Jabatan');

	}

	public function kategorijabatan(){

	return $this->hasOne('App\Kategorijabatan','id','kategorijabatan_id');

	}

	public function statusjabatan(){

	return $this->hasOne('App\Statusjabatan','id','statusjabatan_id');
	
	}

	public function pic(){
	return $this->hasOne('App\Pic','id');
	}
		
}
