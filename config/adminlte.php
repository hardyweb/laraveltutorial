<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => 'AdminLTE 3',
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-favicon
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-logo
    |
    */

    'logo' => '<b>Sistem Tempahan</b>',
    'logo_img' => 'vendor/adminlte/dist/img/AdminLTELogo.png',
    'logo_img_class' => 'brand-image img-circle elevation-3',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'AdminLTE',

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-user-menu
    |
    */

    'usermenu_enabled' => true,
    'usermenu_header' => false,
    'usermenu_header_class' => 'bg-primary',
    'usermenu_image' => false,
    'usermenu_desc' => false,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => null,
    'layout_fixed_navbar' => null,
    'layout_fixed_footer' => null,

    /*
    |--------------------------------------------------------------------------
    | Extra Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#66-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_header' => 'container-fluid',
    'classes_content' => 'container-fluid',
    'classes_sidebar' => 'sidebar-dark-primary elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand-md',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#67-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#68-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#69-urls
    |
    */

    'use_route_url' => false,

    'dashboard_url' => 'dashboard',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password/reset',

    'password_email_url' => 'password/email',

    'profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#610-laravel-mix
    |
    */

    'enabled_laravel_mix' => false,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#611-menu
    |
    */

    'menu' => [

	'MAIN NAVIGATION',
	[ 
		'text' =>'Dashboard',
		'url' => 'dashboard',
		'icon' => 'fas fa-chart-line',
		'can' => 'admin',
	],
	[
		'text' => 'Users',
		'url' => 'user',
		'icon' => 'fas fa-user-friends',
		'can' => 'admin',
	],
	[
		'text' => 'Tempahan',
		'url' => 'tempahan',
		'icon' => 'fas fa-ticket-alt',
		'can' => 'admin',
	],

		[
		'text' => 'Tempahan',
		'url' => 'tempahanuser',
		'icon' => 'fas fa-ticket-alt',
		'can' => 'user',
		],
	[
		'text' => 'Senarai Tempahan',
		'url' => '/tempahanuser/senarai',
		'icon' => 'fas fa-ticket-alt',
		'can' => 'user',
	],



	[
		'text' => 'Senarai Tempahan',
		'url' => '/tempahan/senarai',
		'icon' => 'fas fa-ticket-alt',
		'can' => 'admin',
	],


    [
		'text' => 'Pinjam Aset',
		'url' => 'pinjamaset',
		'icon' => 'fa fa-desktop',
		'can' => 'admin',
	],

	  [
		'text' => 'Pinjam Aset',
		'url' => 'pinjamasetuser',
		'icon' => 'fa fa-desktop',
		'can' => 'user',
	],
	 
    [
        'text'    => 'UTILITI',
	'icon'    => 'fa fa-wrench',
	'can' => 'admin',
        'submenu' => [

	[
		'text' => 'Jabatan',
		'url' => 'jabatan',
		 
	],
	[
		'text' => 'Bilik',
		'url' => 'bilik',
		 
	],
	[
		'text' => 'Tingkat',
		'url' => 'tingkat',
		 
	],
	[
		'text' => 'Premis',
		'url' => 'premis',
		 
	],

	[
		'text' => 'Pic',
		'url' => 'pic',
		 
	],

	[
		'text' => 'Kategori Jabatan',
		'url' => 'kategorijabatan',
		 
	],
	[
		'text' => 'Status Jabatan',
		'url' => 'statusjabatan',
		 
	],
	[
		'text' => 'Nama Kelengkapan',
		'url' => 'namakelengkapan',
		 
    ],
],
],           
    [
        'text'    => 'ASET JABATAN',
	'icon'    => 'fa fa-tasks',
	'can' => 'admin',
        'submenu' => [
            [
                'text' => 'Senarai Aset',
                'url'  => 'aset',
            ],
            [
                'text' => 'Jenama',
                'url'  => 'jenama',
            ],
            [
                'text' => 'Jenis',
                'url'  => 'jenis',
            ],
            [
                'text' => 'Ram',
                'url'  => 'ram',
            ],
            [
                'text' => 'Saiz Monitor',
                'url'  => 'saizmonitor',
            ],
            [
                'text' => 'Display Port',
                'url'  => 'displayport',
	    ],
  	    [
                'text' => 'Sd Card',
                'url'  => 'sdcard',
	    ],
  	    [
                'text' => 'Lensa',
                'url'  => 'lensa',

            ],



        ],
    ],
[
        'text'    => 'UTILITI',
	'icon'    => 'fa fa-tasks',
	'can' => 'user',
        'submenu' => [
            [
                'text' => 'Bilik',
                'url'  => 'bilikuser',
	    ],
	  [
                'text' => 'Tingkat',
                'url'  => 'tingkatuser',
	  ],
	  [
                'text' => 'Pic',
                'url'  => 'picuser',
            ],
           

           

        ],
 ],

 [
        'text'    => 'ASET JABATAN',
	'icon'    => 'fa fa-tasks',
	'can' => 'user',
        'submenu' => [
            [
                'text' => 'Senarai Aset',
                'url'  => 'asetuser',
            ],
           

        ],
 ],
[
        'text'    => 'Laporan',
	'icon'    => 'fa fa-tasks',
	'can' => 'admin',
        'submenu' => [
            [
                'text' => 'Laporan Tempahan',
                'url'  => 'tempahan/laporan',
            ],
           

        ],
    ],




    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#612-menu-filters
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
    //    JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#613-plugins
    |
    */

    'plugins' => [

	    [ 
		'name' => 'TempusDominus',
		'active' => false,
		'files'=> [

			[
				'type' => 'js',
				'asset' => false,
				'location' => '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js',
			],


			[
				'type' => 'js',
				'asset' => false,
				'location' => '//cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/js/tempusdominus-bootstrap-4.min.js',
			],




			[
				'type' => 'css',
				'asset' => false,
				'location' => '//cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/css/tempusdominus-bootstrap-4.min.css',
			],


		],

	],

	[
	     'name' => 'Fullcalendar',
            'active' => false,
	    'files' => [

		 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/bootstrap/main.min.js',
                ],


		 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales-all.min.js',
		 ],

		 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.js',
		 ],
		 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.js',
		 ],
		 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/interaction/main.min.js',
		 ],
		 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.min.js',
		 ],
		 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/moment/main.min.js',
		 ],
		 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/moment-timezone/main.min.js',
		 ],
		 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.js',
		 ],	



             
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/bootstrap/main.min.css',
		],
		 [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css',
		 ],
		 [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css',
		 ],
		 [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.css',
                ],


             
            ],


    ],
    [
        'name' => 'Fullcalendar2',
           'active' => false,
           'files' => [
               [
                   'type' => 'js',
                   'asset' => false,
                   'location' => '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment-with-locales.min.js',
               ],
               [
                'type' => 'js',
                'asset' => false,
                'location' => '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.js',
            ],
            [
                'type' => 'js',
                'asset' => false,
                'location' => '//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js',
            ],
               [
                   'type' => 'css',
                   'asset' => false,
                   'location' => '//fullcalendar.io/js/fullcalendar-3.0.1/fullcalendar.min.css',
               ],
               [
                'type' => 'css',
                'asset' => false,
                'location' =>  '//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css',
              ],
             // [
             //   'type' => 'css',
             //   'asset' => false,
             //   'location' =>  '//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css',
             // ],
            
           ],

        
   ],

	[
	     'name' => 'Toastr',
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css',
                ],
             
            ],


	],
        [
            'name' => 'Datatables',
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
		],
 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js',
 ],
 [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js',
                ],

                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        [
            'name' => 'Select2',
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        [
            'name' => 'Chartjs',
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        [
            'name' => 'Sweetalert2',
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        [
            'name' => 'Pace',
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
