# CSS LETAK DI META ( ATAS )
# JSS LETAK DI BAWAH SEBELUM  `</body>`

# RUJUKAN UTAMA ADMINLTE 

[Admin LTE](https://adminlte.io/themes/dev/AdminLTE/index3.html)


# PEMILIHAN NAVIGATION MENU ( BOOTSTRAP ) 
- Left NAV
- Top Nav 

# PEMILIHAN EDITOR 
- Notepad ++
- PHP Storm 
- VS Code 
- neovim / vim / vi 

Gunakan kelebihan plugin sedia ada dalam vscode seperti emmet, multi cursor dan key binding lain untuk mempercepatkan proses penulisan kod. 

[Emmet Cheet Sheet](https://docs.emmet.io/cheat-sheet/) 
[Contoh Pengguaan Emmet](https://levelup.gitconnected.com/speed-up-your-coding-with-emmet-in-vscode-433162d95880)

[Multi cursor ](https://stackoverflow.com/questions/30037808/multiline-editing-in-visual-studio-code)

# CMDer

- support linux command 
- multi tab terminal 

