# view collection ( dump result)


- $collection->dd()
- dd($collection)
- {{$collection}} dalam blade
- guna php artisan tinker 

# Collection 
- first();
- get();


# Validation Request 
- https://laravel.com/docs/8.x/validation#available-validation-rules

# Date Carbon 

Guna bantuan plugin Carbon untuk tukar format date dari ddmmyyyy kepada YYYY-DD-MM dalam mysql 


cara lama
$tempahan->tarikhakhir = Carbon::createFromFormat('m/d/Y g:ia',$request->input('tarikhtamat'));


cara baru

protected $dates = ['tarikh']; <== buat dalam model 

$tempahan->tarikh = $request->input('tarikh')->format('Y-m-d');

# baca tarikh dari database, tukar terus ke dd/mm/YYYY

public function getTarikhakhirAttribute($value){

	$date = new \Carbon\Carbon($value);
	return $date->format('d/m/Y H:i');
	}


# Tukar Bahasa dalam adminLTE 
config/app.php

Copy folder from ‘resources/lang/vendor/adminlte/en’ to ‘resources/lang/vendor/adminlte/my’




