@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
<h1>Bangsa</h1>
@stop

@section('content')

<!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Senarai Bangsa</h3>

          <div class="card-tools">
		<a href="/bangsa/create">  
	 <button type="button" class="btn btn-tool"  data-toggle="tooltip" title="tambah">
             Tambah </button> </a>
          </div>
        </div>
        <div class="card-body">



  <form role="form" method="post">
   {{ csrf_field()}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama',$bangsa->nama)}}"  placeholder="Masukkan Nama">
                  </div>
                 
                
                <!-- /.card-body -->

                <div class="card-footer">
		  <button type="submit" class="btn btn-primary">Submit</button>
		   <a href="{{ url()->previous() }}" class="btn btn-default">Back</a>
                </div>
              </form>


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@stop

@section ('plugins.Toastr',true)

@section('js')
@include('partials.notification');

@stop

