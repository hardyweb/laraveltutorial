@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
<h1>Bangsa</h1>
@stop

@section('content')

<!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Senarai Bangsa</h3>

          <div class="card-tools">
		<a href="/bangsa/create">  
	 <button type="button" class="btn btn-tool"  data-toggle="tooltip" title="tambah">
             Tambah </button> </a>
          </div>
        </div>
        <div class="card-body">

<table id="basicTable" class="table dataTable" data-form="deleteForm">
                <thead>
                <tr>
                    <th>#</th>
                     <th>Jantina</th>
		    
                     <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($bangsas as $bangsa)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                       
                        <td>{{$bangsa->nama}}</td>
                       
                        <td class="table-action text-nowrap text-center">

                        <a class="btn btn-info btn-sm" href="/bangsa/{{$bangsa->id}}/edit">Edit</a>
		        <a class="btn btn-danger btn-circle delete btn-sm" href="/bangsa/{{ $bangsa->id }}/destroy">Delete</a>
                         
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
		


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@stop

@section ('plugins.Toastr',true)
@section ('plugins.Sweetalert2',true)
@section('js')
@include('partials.notification');
<script>
 

 $('.delete').on('click', function (event){
	  event.preventDefault();
	  const url = $(this).attr('href'); 
	  swal.fire({
	     title: 'Are you sure?',
	     text: 'This record and it`s details will be permanantly deleted!',
	     type: 'warning',
	      showCancelButton: true,
	      confirmButtonClass: 'btn-danger',
	      confirmButtonText : 'Yes',
	     preConfirm:false 
	}).then((result) => {



		if(result.value){

		window.location.href = url;


		console.log('berjaya');
		}else if(result.dismiss === swal.DismissReason.cancel)
		{
		
		}
	  });
  });

 

 </script>
@stop

