@extends('adminlte::page')
@section('title', 'Bilik')
@section('content_header')
    <h1>Tambah Bilik</h1>
@stop
@section('content')

  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">	

	</div>


	<div class="box-body">

        <!--Form -->
<!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Bilik Baru</h3>
              </div>
	      <!-- /.card-header -->
		
	       <div class="row">
	       <div class="col-lg-6">
		       <!-- form start -->

	



	       
	      @can('admin')
	      <form role="form" method="post" action="/bilik/new" >
	      @elsecan('user')
	      <form role="form" method="post" action="/bilikuser/new" >

	      @endcan
		{{csrf_field()}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama')}}" placeholder="nama">
                  </div>
                  <div class="form-group">
                    <label for="kapasiti">Kapasiti</label>
		    <input type="text" class="form-control" id="kapasiti" name="kapasitimax"
value="{{old('kapasitimax')}}" placeholder="Kapasiti Maximum">

		  </div>

		  <div class="form-group">
                    <label for="Alamat">Alamat</label>
                   <textarea id="alamat" name="alamat" class="form-control" cols="10" rows="10" value="{{old('alamat')}}"></textarea>
		  </div>
		@cannot('user')
		 <div class="form-group">
		    <label for="">Jabatan</label>
		    <select id="jabatan" class="form-control" name="jabatan">
			<option value="">Sila Pilih</option>
			@foreach ($jabatans as $jabatan)
				<option value="{{$jabatan->id}}" 
					@if (old('jabatan') == $jabatan->id) selected @endif > {{$jabatan->nama}} </option>
			@endforeach
				
		    </select>
		   

                  </div>
                  @endcannot                
                </div>
		</div>
		<div class="col-xs-12 m-3">


		<div class="form-group">
		    <label for="">Tingkat </label>
		    <select id="tingkat" class="form-control" name="tingkat">
			<option value="">Sila Pilih</option>
			@foreach ($tingkats as $tingkat)
				<option value="{{$tingkat->id}}" 
					@if (old('tingkat') == $tingkat->id) selected @endif > {{$tingkat->premis->nama}}- {{$tingkat->nama}} </option>
			@endforeach
				
		    </select>
		 </div>

		 <div class="form-group">
		    <label for="">Pic </label>
		    <select id="pic" class="form-control" name="pic">
			<option value="">Sila Pilih</option>
			@foreach ($pics as $pic)
				<option value="{{$pic->id}}" 
					@if (old('pic') == $pic->id) selected @endif > {{$pic->nama}} </option>
			@endforeach
				
		    </select>
		 </div>

		 <div class="form-group">
		    <label for="">Kegunaan</label>
		    <select id="kegunaan" class="form-control" name="kegunaan">
			<option value="">Sila Pilih</option>
			@foreach ($kegunaans as $kegunaan)
				<option value="{{$kegunaan->id}}" 
					@if (old('kegunaan') == $kegunaan->id) selected @endif > {{$kegunaan->nama}} </option>
			@endforeach
				
		    </select>
		 </div>

		<div class="form-group">
		<label for="aktif">Aktif</label>
		<select id="aktif" class="form-control" name="aktif">

		<option value="1" selected>Aktif</option>
		<option value="2">Tidak Aktif</option>
		</select>
		</div>

		<button type="submit" class="btn btn-primary">Submit</button>

		
		</div>
		<!-- /.card-body -->
		
                <div class="card-footer">
                  
                </div>
	      </form>
	
		</div>
			
            </div>
            <!-- /.card -->
		

<!-- Forizes -->
	   </div>

	</div>
      
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
@stop

@section ('plugins.Select2', true)
@section ('plugins.Toastr',true)

@section('js')
@include('partials.notification');

<script>
  $(document).ready( function () {
	      $('#jabatan').select2();
  });


  </script>
@stop

