<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
		$table->id();
		$table->string('name');
		$table->string('nirc');  //National Registration Identity Card Number 
		$table->text('address');
		$table->integer('poscode');
		$table->date('dob');
		$table->foreignId('state_id')->constrained();
		$table->foreignId('position_id')->constrained();
		$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
