# TENTATIF

https://gitlab.com/hardyweb/laraveltutorial/-/blob/master/rancanganmengajar.md

# SETUP WORKING ENVIRONMENT

Download Laragon – Full 

https://nodejs.org/dist/v14.16.1/node-v14.16.1-x64.msi
 

https://jaist.dl.sourceforge.net/project/laragon/releases/4.0/laragon-full.exe

 
+ update php 8.1
https://windows.php.net/downloads/releases/php-8.1.10-nts-Win32-vs16-x64.zip

+ update composer 2.4

composer self-update


add path .. 

install node version 14


Download Vscode
https://az764295.vo.msecnd.net/stable/3c4e3df9e89829dce27b7b5c24508306b151f30d/VSCodeUserSetup-x64-1.55.2.exe



Install Plugin 

https://devdojo.com/bobbyiliev/8-awesome-vs-code-extensions-for-laravel-developers
https://realarafatben.medium.com/10-awesome-vs-code-extensions-for-php-laravel-developers-913a368bc896

PHP Intelephense 
Laravel Intellisense



![alt text](https://gitlab.com/hardyweb/laraveltutorial/-/raw/master/Capture.JPG "Gambar ")

composer create-project laravel/laravel –prefer-dist  namaprojek 

composer create-project laravel/laravel:^7.0 –prefer-dist  namaprojek 

composer require laravel/ui

php artisan ui bootstrap --auth

npm install

npm run dev

composer require jeroennoten/laravel-adminlte

php artisan adminlte:install

https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Usage



Setting login as default page 

## SETUP GIT

https://gitlab.com/hardyweb/laraveltutorial/-/blob/master/setupgit

## PENGENALAN LARAVEL 

https://gitlab.com/hardyweb/laraveltutorial/-/blob/master/pengetahuanam.md

https://gitlab.com/hardyweb/laraveltutorial/-/blob/master/arahanlinux.md


## LARAVEL CONFIG 

1. env 

2. adminlte config

## ARAHAN LARAVEL & VSCODE SHORTCUT KEY

https://gitlab.com/hardyweb/laraveltutorial/-/blob/master/arahanlaravel.md

https://gitlab.com/hardyweb/laraveltutorial/-/blob/master/Laraveltips.md

https://www.c-sharpcorner.com/article/visual-studio-code-keyboard-shortcut-for-windows/

## ERD 

https://gitlab.com/hardyweb/laraveltutorial/-/tree/master/erd

## DEVELOPMENT

+ Create Model 
+ Add migration file 
+ Migrate model
+ create controller / edit controller
+ create Route 
+ create resource view 

## LARAVEL BEST PRACTICE

https://dev.to/lathindu1/laravel-best-practice-coding-standards-part-01-304l

https://github.com/alexeymezenin/laravel-best-practices

## SELINGAN 

https://www.youtube.com/watch?v=nVwJP3VwfZQ


https://gitlab.com/hardyweb/laraveltutorial/-/blob/master/selingan/test.gif

 

