<?php

use App\Http\Controllers\BangsaController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   // return view('welcome');
   return view('auth.login');
});

Auth::routes([
   'register' => false,  
   'reset' => false, // Password Reset Routes...
   'verify' => false, // Email Verification Routes...
]);

 
Route::group(['middleware' => ['auth']], function () {

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
  
   Route::get('/state',[App\Http\Controllers\StateController::class,'index']);
   Route::get('/state/create',[App\Http\Controllers\StateController::class,'create']);
   Route::post('/state/create',[App\Http\Controllers\StateController::class,'store']);
    Route::get('/state/{id}/view',[App\Http\Controllers\StateController::class,'view']);
    Route::get('/state/{id}/edit',[App\Http\Controllers\StateController::class,'edit']);
    Route::post('/state/{id}/edit',[App\Http\Controllers\StateController::class,'update']);
    Route::get('/state/{id}/delete',[App\Http\Controllers\StateController::class,'destroy']);

Route::resource('users',UsersController::class);


Route::prefix('dev')->group(function() {

Route::get('/clearcache', function(){

Artisan::call('optimize:clear');
return nl2br(Artisan::outpu());
});


});




});
